package org.academiadecodigo.proxymorons.pooprunner;

import org.academiadecodigo.simplegraphics.graphics.Canvas;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.awt.*;

public class Grid {
    private int width, height;
    //private final int PADDING;

    public Grid() {
        width = 1290;
        height = 710;
        //PADDING = 10;

        Canvas.setMaxX(width);
        Canvas.setMaxY(height);
        Rectangle grid = new Rectangle(0, 0, width, height);

        grid.draw();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
