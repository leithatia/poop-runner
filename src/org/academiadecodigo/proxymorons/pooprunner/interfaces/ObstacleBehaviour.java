package org.academiadecodigo.proxymorons.pooprunner.interfaces;

public interface ObstacleBehaviour {
    public void spawn();
    public void move();
}
