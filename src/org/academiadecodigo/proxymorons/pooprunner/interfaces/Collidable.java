package org.academiadecodigo.proxymorons.pooprunner.interfaces;

import org.academiadecodigo.proxymorons.pooprunner.Player;
import org.academiadecodigo.proxymorons.pooprunner.Obstacle;

public interface Collidable {
    public boolean detectCollision(Player player, Obstacle obstacle);
}
