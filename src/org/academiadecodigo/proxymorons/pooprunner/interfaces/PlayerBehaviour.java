package org.academiadecodigo.proxymorons.pooprunner.interfaces;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

public interface PlayerBehaviour {
    public void jump() throws UnsupportedAudioFileException, LineUnavailableException, IOException;
}
