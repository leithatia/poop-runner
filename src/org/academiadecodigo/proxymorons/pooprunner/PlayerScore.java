package org.academiadecodigo.proxymorons.pooprunner;

import org.academiadecodigo.simplegraphics.graphics.Text;

public class PlayerScore {
    public static int playerScore = 0;
    private static long elapsedTime = System.currentTimeMillis();


    public static void updateScore() {
        if(System.currentTimeMillis() - elapsedTime > 100) {
            playerScore++;
            elapsedTime = System.currentTimeMillis();

        }

    }

    public static void resetScore() {
        playerScore = 0;
    }

}
