package org.academiadecodigo.proxymorons.pooprunner;

import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.awt.*;
import java.util.Iterator;
import java.util.LinkedList;

// Layer 6 is the furthest layer at the back

public class ParallaxBackground {
    private final int DISPLAY_WIDTH;
    Picture[][] layers = new Picture[6][2];

    public ParallaxBackground(int width) {
        DISPLAY_WIDTH = width;
        initLayers();
    }

    public void update() {

        double speedMultiplier = 1.5; // default 1

        for (Picture[] layer : layers) {
            for (Picture picture : layer) {
                double BASE_SCROLL_SPEED = -0.5;
                picture.translate(BASE_SCROLL_SPEED * speedMultiplier, 0);
            }

            if (layer[0].getX() + DISPLAY_WIDTH <= 0) {
                layer[0].translate(DISPLAY_WIDTH * 2, 0);
            }

            if (layer[1].getX() + DISPLAY_WIDTH <= 0) {
                layer[1].translate(DISPLAY_WIDTH * 2, 0);
            }

            speedMultiplier += 1.5;
        }
    }

    private void initLayers() {
        String[] filePaths = {
                ResourceHandler.PREFIX + "bg_6.png",
                ResourceHandler.PREFIX + "bg_5.png",
                ResourceHandler.PREFIX + "bg_4.png",
                ResourceHandler.PREFIX + "bg_3.png",
                ResourceHandler.PREFIX + "bg_2.png",
                ResourceHandler.PREFIX + "bg_1.png"
        };

        for (int i = 0; i < layers.length; i++) {
            int nextXPos = 0;

            for (int j = 0; j < layers[i].length; j++) {
                layers[i][j] = new Picture(nextXPos, 0, filePaths[i]);
                layers[i][j].draw();
                nextXPos += DISPLAY_WIDTH;
            }
        }
    }
}
