package org.academiadecodigo.proxymorons.pooprunner;

import javax.sound.sampled.*;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class SoundManager {

    // Intellij code
/*
    public static void playJumpSound() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        try {
            String filePath = ResourceHandler.PREFIX + "player_jump_0" + SoundManager.randomIndex(9) + "_SFX.wav";
            AudioInputStream as = AudioSystem.getAudioInputStream(new File(filePath));
            Clip clip = AudioSystem.getClip();
            clip.open(as);
            clip.start();
            //clip.close();
            as.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void playPlayerDeathSound() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        try {
            String filePath = ResourceHandler.PREFIX + "player_scream_0" + SoundManager.randomIndex(4) + "_SFX.wav";
            AudioInputStream as = AudioSystem.getAudioInputStream(new File(filePath));
            Clip clip = AudioSystem.getClip();
            clip.open(as);
            clip.start();
            //clip.close();
            as.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void playPoopDeathSound() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        try {
            String filePath = ResourceHandler.PREFIX + "poop_death_0" + SoundManager.randomIndex(4) + "_SFX.wav";
            AudioInputStream as = AudioSystem.getAudioInputStream(new File(filePath));
            Clip clip = AudioSystem.getClip();
            clip.open(as);
            clip.start();
            //clip.close();
            as.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void playAmbientSound() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        try {
            String filePath = ResourceHandler.PREFIX + "forest_AMB.wav";
            AudioInputStream as = AudioSystem.getAudioInputStream(new File(filePath));
            final Clip clip = AudioSystem.getClip();
            clip.open(as);
            clip.loop(Clip.LOOP_CONTINUOUSLY);
            clip.start();

            clip.addLineListener(new LineListener() {
                @Override
                public void update(LineEvent event) {
                    if (event.getType() == LineEvent.Type.STOP) {
                        clip.close();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/

    // Jar code
    public static void playJumpSound() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        try {
            String filePath = "/" + ResourceHandler.PREFIX + "player_jump_0" + SoundManager.randomIndex(9) + "_SFX.wav";

            InputStream is = SoundManager.class.getResourceAsStream(filePath);

            if (is == null) {
                throw new RuntimeException("Resource not found: " + filePath);
            }

            InputStream bufferedIn = new BufferedInputStream(is);
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(bufferedIn);
            final Clip clip = AudioSystem.getClip();
            clip.open(inputStream);
            clip.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void playPlayerDeathSound() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        try {
            String filePath = "/" + ResourceHandler.PREFIX + "player_scream_0" + SoundManager.randomIndex(4) + "_SFX.wav";
            InputStream is = SoundManager.class.getResourceAsStream(filePath);

            if (is == null) {
                throw new RuntimeException("Resource not found: " + filePath);
            }

            InputStream bufferedIn = new BufferedInputStream(is);
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(bufferedIn);
            final Clip clip = AudioSystem.getClip();
            clip.open(inputStream);
            clip.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void playPoopDeathSound() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        try {
            String filePath = "/" + ResourceHandler.PREFIX + "poop_death_0" + SoundManager.randomIndex(4) + "_SFX.wav";
            InputStream is = SoundManager.class.getResourceAsStream(filePath);

            if (is == null) {
                throw new RuntimeException("Resource not found: " + filePath);
            }

            InputStream bufferedIn = new BufferedInputStream(is);
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(bufferedIn);
            final Clip clip = AudioSystem.getClip();
            clip.open(inputStream);
            clip.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void playAmbientSound() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        try {
            String filePath = "/" + ResourceHandler.PREFIX + "forest_AMB.wav";
            //final Clip clip = AudioSystem.getClip();

            InputStream is = SoundManager.class.getResourceAsStream(filePath);
            if (is == null) {
                throw new RuntimeException("Resource not found: " + filePath);
            }

            InputStream bufferedIn = new BufferedInputStream(is);
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(bufferedIn);
            final Clip clip = AudioSystem.getClip();
            clip.open(inputStream);
            clip.loop(Clip.LOOP_CONTINUOUSLY);
            clip.start();

            clip.addLineListener(new LineListener() {
                @Override
                public void update(LineEvent event) {
                    if (event.getType() == LineEvent.Type.STOP) {
                        clip.close();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static int randomIndex(int max) {
        return (int) (Math.random() * max);
    }
}