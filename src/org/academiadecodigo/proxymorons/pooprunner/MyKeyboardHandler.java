package org.academiadecodigo.proxymorons.pooprunner;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;


public class MyKeyboardHandler implements KeyboardHandler {

    private Keyboard keyboard;
    private Player player;

    public MyKeyboardHandler(Player player) {
        this.player = player;
    }

    public void init() {
        keyboard = new Keyboard(this);

        // add new keys here, you don't need to change the rest of the code
        int[] keys = new int[]{
                KeyboardEvent.KEY_SPACE
        };

        // creates an event, sets the keys and tells the keyboard to listen for the event
        for (int key : keys) {
            KeyboardEvent event = new KeyboardEvent();
            event.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            event.setKey(key);
            keyboard.addEventListener(event);
        }

    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        if (!player.isPlaying()) {
            player.toggleIsPlaying();
        } else {
            if (keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE) {
                if (player.isJumping()) {
                    return;
                } else {
                    try {
                        player.jump();
                    } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}


