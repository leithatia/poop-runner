package org.academiadecodigo.proxymorons.pooprunner;

import org.academiadecodigo.simplegraphics.graphics.Canvas;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws InterruptedException, UnsupportedAudioFileException, LineUnavailableException, IOException {
        Game game = new Game();
        //Canvas.setMaxX();
        game.initGame();
        game.newGame();
    }
}
