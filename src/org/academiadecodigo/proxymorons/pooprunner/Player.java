package org.academiadecodigo.proxymorons.pooprunner;

import org.academiadecodigo.proxymorons.pooprunner.interfaces.PlayerBehaviour;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

public class Player implements PlayerBehaviour {

    private final int JUMP_SPEED = -8;
    private final int FALL_SPEED = 15;
    private int imageXStartPos, imageYStartPos, collisionBoxXStartPos, collisionBoxYStartPos;
    Position pos;
    Rectangle collisionBox;
    Picture jumpOrFallImage;
    PlayerImage playerImage;
    private boolean isFalling, isJumping;
    private AnimationState animationState;
    private boolean isPlaying;


    public Player(Grid grid) {
        imageXStartPos = 200;
        imageYStartPos = 320;
        collisionBoxXStartPos = imageXStartPos + 40;
        collisionBoxYStartPos = imageYStartPos + 210;

        pos = new Position(imageXStartPos, imageYStartPos);
        animationState = AnimationState.RUNNING;

        playerImage = new PlayerImage(imageXStartPos, imageYStartPos);
        jumpOrFallImage = new Picture(imageXStartPos, imageYStartPos, ResourceHandler.PREFIX + "Jump__002.png");
        collisionBox = new Rectangle(collisionBoxXStartPos, collisionBoxYStartPos, 120, 50);

        collisionBox.setColor(Color.WHITE);
        //collisionBox.fill();
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void toggleIsPlaying() {
        isPlaying = !isPlaying;
    }

    public boolean isJumping() {
        return isJumping;
    }

    public void update() throws InterruptedException {
        int JUMP_HEIGHT = 50;

        if (isJumping) {
            moveUp();
        }

        if (jumpOrFallImage.getY() < JUMP_HEIGHT) {
            isJumping = false;
            isFalling = true;
        }

        if (jumpOrFallImage.getY() >= imageYStartPos) {
            isFalling = false;
        }

        if (isFalling) {
            moveDown();
        }

        if (!isJumping && !isFalling) {
            playerImage.show();
            jumpOrFallImage.delete();
            playerImage.playerRunningAnimation();
        } else {
            playerImage.hide();
        }
    }

    @Override
    public void jump() throws UnsupportedAudioFileException, LineUnavailableException, IOException {
        if (imageYStartPos == jumpOrFallImage.getY()) {
            isJumping = true;
            SoundManager.playJumpSound();
        }
    }

    public void moveUp() {
        jumpOrFallImage.load(ResourceHandler.PREFIX + "Jump__002.png");
        jumpOrFallImage.draw();
        jumpOrFallImage.translate(0, JUMP_SPEED);
        collisionBox.translate(0, JUMP_SPEED);
    }

    public void moveDown() {
        int imageYOffset = imageYStartPos - jumpOrFallImage.getY(); // in case image goes below y start pos
        int collisionYOffset = collisionBoxYStartPos - collisionBox.getY(); // same for collision box

        jumpOrFallImage.translate(0, jumpOrFallImage.getY() + FALL_SPEED > 320 ? imageYOffset : FALL_SPEED);
        jumpOrFallImage.load(ResourceHandler.PREFIX + "Jump__009.png");
        jumpOrFallImage.draw();
        collisionBox.translate(0, jumpOrFallImage.getY() + FALL_SPEED > 320 ? collisionYOffset : FALL_SPEED);
    }
}
