package org.academiadecodigo.proxymorons.pooprunner;

import org.academiadecodigo.proxymorons.pooprunner.interfaces.Collidable;

public class CollisionDetector implements Collidable {

    //returns true if collision detected
    public boolean detectCollision(Player player, Obstacle obstacle) {
        return player.collisionBox.getX() + player.collisionBox.getWidth() > obstacle.pos.getX() &&
                obstacle.pos.getX() + obstacle.getWidth() > player.collisionBox.getX() &&
                player.collisionBox.getY() + player.collisionBox.getHeight() > obstacle.pos.getY() &&
                obstacle.pos.getY() + obstacle.getHeight() > player.collisionBox.getY();
    }
}
