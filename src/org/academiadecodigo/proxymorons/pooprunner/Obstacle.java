package org.academiadecodigo.proxymorons.pooprunner;

import org.academiadecodigo.proxymorons.pooprunner.interfaces.ObstacleBehaviour;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Obstacle implements ObstacleBehaviour {
    private int width, height, xStartPos, yStartPos, xPoop, yPoop, xLastPoopPos;
    private int frameIndex;
    private long lastFrameTime;
    Position pos;
    Rectangle obstacleBox;
    private boolean obstacleExists;
    public static long lastSpawnTime;
    private Picture[] poopsImages = new Picture[4];
    private Picture[] deadPoopsImages = new Picture[6];

    public Obstacle(Grid grid) {
        width = 30;
        height = 70;
        xStartPos = grid.getWidth() - width + 25;
        yStartPos = grid.getHeight() - height - 120;
        xPoop = xStartPos - 20;
        yPoop = yStartPos - 10;

        initPoop();
        obstacleExists = true;
        pos = new Position(xStartPos, yStartPos);
        obstacleBox = new Rectangle(xStartPos, yStartPos, width, height);
        //obstacleBox.fill();

        lastSpawnTime = System.currentTimeMillis();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Picture[] getDeadPoopsImages() {
        return deadPoopsImages;
    }

    @Override
    public void spawn() {}

    @Override
    public void move() {

        for (int i = 0; i < poopsImages.length; i++){
            if(poopsImages[i] != null) {
                poopsImages[i].translate(-5, 0);
            }
        }
        obstacleBox.translate(-5, 0);
        pos.setPos(pos.getX() - 5, yStartPos);

        if (pos.getX() < -50) {
            obstacleExists = false;
            obstacleBox.delete();
        }
    }
    private void initPoop() {

        for(int i = 0; i < poopsImages.length; i++) {
            poopsImages[i] = new Picture(xPoop, yPoop, ResourceHandler.PREFIX + "poop__" + i + ".png");

        }
       // poopsImages[frameIndex].draw();
    }
    public void animatePoop() {
        long currentTime = System.currentTimeMillis();
        long elapsedTime = currentTime - lastFrameTime;
        int frameUpdateInterval = 100;

        if(elapsedTime >= frameUpdateInterval) {
            poopsImages[frameIndex].delete();
            frameIndex = (frameIndex + 1) % 4;
            poopsImages[frameIndex].draw();
            lastFrameTime = currentTime;

        }
    }

    public void deleteLastPoopFrame() {
        poopsImages[frameIndex].delete();
        xLastPoopPos = pos.getX();

        for (int i = 0; i < deadPoopsImages.length; i++) {
            deadPoopsImages[i] = new Picture(xLastPoopPos, yPoop - 40, ResourceHandler.PREFIX + "poop_die__00" + i + ".png");
        }}
}
