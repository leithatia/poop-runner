package org.academiadecodigo.proxymorons.pooprunner;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Text;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.util.Arrays;

public class Game {
    CollisionDetector collisionDetector = new CollisionDetector();
    Grid grid;
    Player player;
    Obstacle obstacle;
    private long elapsedTime;
    private int playerScore;
    ParallaxBackground parallaxBackground;
    public static final int DISPLAY_WIDTH = 1290;
    public static final int DISPLAY_HEIGHT = 710;
    public static boolean gameOver;
    private final int maxNumOfObstacles = 6;
    private boolean isFirstGame = true;
    private final Obstacle[] obstacles = new Obstacle[maxNumOfObstacles];
    private Text scoreText;
    private Text spaceToStartText;
    Obstacle killerPoop; // Poop that kills the player


    public void initGame() throws InterruptedException {
        scoreText = new Text(1200,50,"");
        scoreText.grow(40,30);
        spaceToStartText = new Text(630, 200, "Press space to start...");
        spaceToStartText.grow(250, 100);

        grid = new Grid();
        parallaxBackground = new ParallaxBackground(DISPLAY_WIDTH);
        player = new Player(grid);
        MyKeyboardHandler myKeyboardHandler = new MyKeyboardHandler(player);
        myKeyboardHandler.init();

        spaceToStartText.draw();

        while (!player.isPlaying()) {
            Thread.sleep(10);
        }

        spaceToStartText.delete();

        obstacles[0] = new Obstacle(grid);
    }

    public void newGame() throws InterruptedException, UnsupportedAudioFileException, LineUnavailableException, IOException {
        gameOver = false;
        startGame();
    }

    public void startGame() throws InterruptedException, UnsupportedAudioFileException, LineUnavailableException, IOException {
        if (isFirstGame) {
            SoundManager.playAmbientSound();
        }
        elapsedTime = System.currentTimeMillis();
        playerScore = 0;


        while (!gameOver) {
            // start time for the current frame
            long startTime = System.currentTimeMillis();

            parallaxBackground.update();
            player.update();
            updateObstacles();
            displayScore();
            PlayerScore.updateScore();
            updatePoop();


            gameOver = checkForCollision();

            long frameTime = System.currentTimeMillis() - startTime;
            int TARGET_FPS = 60;
            int FRAME_INTERVAL = 1000 / TARGET_FPS;
            long sleepTime = FRAME_INTERVAL - frameTime;

            if (sleepTime > 0) {
                Thread.sleep(sleepTime);
            }
        }

        stopGame();
    }

    private boolean checkForCollision() throws InterruptedException {
        for (Obstacle ob : obstacles) {
            if ( ob != null) {
                //System.out.println("Player y: " + player.pos.getY() + " Ob y: " + ob.pos.getY());
                if (collisionDetector.detectCollision(player, ob)) {
                    killerPoop = ob;

                    killerPoop.deleteLastPoopFrame();

                    return true;
                }
            }
        }
        return false;
    }
    public void stopGame() throws InterruptedException, UnsupportedAudioFileException, LineUnavailableException, IOException {
        player.toggleIsPlaying();
        SoundManager.playPlayerDeathSound();
        SoundManager.playPoopDeathSound();
        Arrays.fill(obstacles, null);
        player.jumpOrFallImage.delete();
        player.playerImage.startPlayerDiesAnimation(killerPoop);
        PlayerScore.resetScore();
        isFirstGame = false;

        // temp pause after game over
        //Thread.sleep(2000);
        initGame();
        newGame();
    }

    private void updateObstacles() {
        if (!obstaclesFull()) {
            if (System.currentTimeMillis() - Obstacle.lastSpawnTime > (900 + (Math.random()* 10000))) {
                obstacleAdd();
            }
        }

        for (int i = 0; i < obstacles.length; i++) {

            if (obstacles[i] != null) {
                obstacles[i].move();
                if(obstacles[i].pos.getX() < -100) {
                    obstacles[i] = null;
                }
            }
        }
    }

    private boolean obstaclesFull() {
        for (Obstacle ob : obstacles) {
            if (ob == null) {
                return false;
            }
        }
        return true;
    }

    private void obstacleAdd() {
        Obstacle newObstacle = new Obstacle(grid);
        for (int i = 0; i < obstacles.length; i++) {
            if (obstacles[i] == null) {
                obstacles[i] = newObstacle;
                break;
            }
        }
    }

    private void updateScore() {
        if(System.currentTimeMillis() - elapsedTime > 100) {
            playerScore++;
            elapsedTime = System.currentTimeMillis();
        }
    }

    private void updatePoop() {
        for (Obstacle value : obstacles) {
            if (value != null) {
                value.animatePoop();
            }
        }
    }
    public void displayScore(){
        //text.setText(String.valueOf(PlayerScore.playerScore));
        scoreText.setText(String.format("%05d", PlayerScore.playerScore));
        scoreText.draw();
        scoreText.setColor(Color.DARK_GRAY);
    }
}
