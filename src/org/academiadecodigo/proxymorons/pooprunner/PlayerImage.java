package org.academiadecodigo.proxymorons.pooprunner;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class PlayerImage {
    private Picture[] playerAnimation, jumpAnimation, deadAnimation;
    private final int startXPos;
    private final int startYPos;
    private long lastFrameTime;
    private int frameIndex;

    public PlayerImage(int startXPos, int startYPos) {
        this.startXPos = startXPos;
        this.startYPos = startYPos;

        lastFrameTime = System.currentTimeMillis();

        initLayersPlayerRun();
    }

    private void initLayersPlayerRun() {
        playerAnimation = new Picture[10];

        for (int i = 0; i < 10; i++) {
            playerAnimation[i] = new Picture(startXPos, startYPos, ResourceHandler.PREFIX + "Run__00" + i + ".png");
        }

        playerAnimation[0].draw();
    }

    public void updateAnimation(Picture[] playerAnimation) {
        long currentTime = System.currentTimeMillis();
        long elapsedTime = currentTime - lastFrameTime;
        int frameUpdateInterval = 70;

        if (elapsedTime >= frameUpdateInterval) {
            playerAnimation[frameIndex].delete();
            frameIndex = (frameIndex + 1) % 10;
            playerAnimation[frameIndex].draw();
            lastFrameTime = currentTime;
        }
    }

    public void playerRunningAnimation() {
        updateAnimation(playerAnimation);
    }
    public void startPlayerDiesAnimation(Obstacle poop) throws InterruptedException {
        playerAnimation[frameIndex].delete();
        int xDeathPos = startXPos;

        // load images
        for (int i = 0; i < 10; i++) {
            playerAnimation[i] = new Picture(startXPos, startYPos, ResourceHandler.PREFIX + "Dead__00" + i + ".png");
        }

        // animate player and poop death
        for (int i = 0; i < 10; i++) {
            playerAnimation[i].translate(xDeathPos,0);
            playerAnimation[i].draw();
            xDeathPos = startXPos + 5;

            if (i < poop.getDeadPoopsImages().length) {
                poop.getDeadPoopsImages()[i].draw();
            }

            Thread.sleep(100);
            if (i != 9) {
                if (i < poop.getDeadPoopsImages().length - 1) {
                    poop.getDeadPoopsImages()[i].delete();
                }
                playerAnimation[i].delete();
            }
        }
    }

    public void hide() {
        playerAnimation[frameIndex].delete();
    }

    public void show() {
        playerAnimation[frameIndex].draw();
    }
}
